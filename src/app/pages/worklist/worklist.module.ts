import { NgModule } from '@angular/core';
import { NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule, NbRadioModule, 
  NbCheckboxModule, NbButtonModule, NbSelectModule, NbOptionModule } from '@nebular/theme';
import { DataTablesModule } from 'angular-datatables';
import { ThemeModule } from '../../@theme/theme.module';
import { WorklistRoutingModule, routedComponents } from './worklist-routing.module';
import { NgSelectModule } from "@ng-select/ng-select";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedDirectiveModule } from "../../../../src/app/shared-directive/shared-directive.module";
// import { FsIconComponent } from './tree-grid/tree-grid.component';

@NgModule({
  imports: [
    NbCardModule, NbTreeGridModule, NbIconModule, NbInputModule, NbRadioModule, 
    NbCheckboxModule, NbButtonModule, NbSelectModule, NbOptionModule, ThemeModule,
    NgSelectModule,
    FormsModule,ReactiveFormsModule,
    DataTablesModule,
    SharedDirectiveModule,
    WorklistRoutingModule,
  ],
  declarations: [
    ...routedComponents,
    // FsIconComponent,
  ],
})
export class WorklistModule { }
