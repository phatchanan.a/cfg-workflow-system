import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WorklistComponent } from './worklist.component';

import { ProductTemplateComponent } from './template/product/product.component';
import { TasklistTemplateComponent } from './template/tasklist/tasklist.component';

import { HistoryListComponent } from './history/list/list.component';

// import { SupplierTasklistComponent } from './supplier/tasklist/tasklist.component';
// import { SupplierProductComponent } from './supplier/product/product.component';

// import { BuyerTasklistComponent } from './buyer/tasklist/tasklist.component';
// import { BuyerProductComponent } from './buyer/product/product.component';

// import { EvpTasklistComponent } from './buyer-evp/tasklist/tasklist.component';
// import { EvpProductComponent } from './buyer-evp/product/product.component';

// import { MgrTasklistComponent } from './buyer-mgr/tasklist/tasklist.component';
// import { MgrProductComponent } from './buyer-mgr/product/product.component';

// import { BysTasklistComponent } from './bys/tasklist/tasklist.component';
// import { BysProductComponent } from './bys/product/product.component';

// import { CfmTasklistComponent } from './merchandise/tasklist/tasklist.component';
// import { CfrTasklistComponent } from './merchandise/tasklist/tasklist.component';
// import { CmkTasklistComponent } from './merchandise/tasklist/tasklist.component';
// import { TopsTasklistComponent } from './merchandise/tasklist/tasklist.component';
// import { CfmProductComponent } from './merchandise/product/product.component';
// import { CfrProductComponent } from './merchandise/product/product.component';
// import { CmkProductComponent } from './merchandise/product/product.component';
// import { TopsProductComponent } from './merchandise/product/product.component';

// import { QaTasklistComponent } from './qa/tasklist/tasklist.component';
// import { QaProductComponent } from './qa/product/product.component';

const routes: Routes = [{
  path: '',
  component: WorklistComponent,
  children: [
    {
      path: 'history',
      component: HistoryListComponent,
      data: {test:"test"}
    },
    {
      path: ':moduleName',
      component: TasklistTemplateComponent,
      data: {test:"test"}
    },
    {
      path: ':moduleName/product/:id',
      component: ProductTemplateComponent,
      data: {test:"test"}
    },

    // {
    //   path: ':_moduleName/:moduleName',
    //   component: TasklistTemplateComponent,
    //   data: {test:"test"}
    // },    
    // {
    //   path: ':_moduleName/:moduleName/product/:id',
    //   component: ProductTemplateComponent,
    //   data: {test:"test"}
    // },


    // {
    //   path: 'supplier',
    //   component: SupplierTasklistComponent,
    // },
    // {
    //   path: 'supplier/product',
    //   component: SupplierProductComponent,
    // },

    // {
    //   path: 'buyer',
    //   component: BuyerTasklistComponent,
    // },
    // {
    //   path: 'buyer/product',
    //   component: BuyerProductComponent,
    // },

    // {
    //   path: 'evp',
    //   component: EvpTasklistComponent,
    // },
    // {
    //   path: 'evp/product',
    //   component: EvpProductComponent,
    // },

    // {
    //   path: 'mgr',
    //   component: MgrTasklistComponent,
    // },
    // {
    //   path: 'mgr/product',
    //   component: MgrProductComponent,
    // },

    // {
    //   path: 'bys',
    //   component: BysTasklistComponent,
    // },
    // {
    //   path: 'bys/product',
    //   component: BysProductComponent,
    // },

    // {
    //   path: 'merchandise/cfm',
    //   component: CfmTasklistComponent,
    // },
    // {
    //   path: 'merchandise/cfr',
    //   component: CfrTasklistComponent,
    // },
    // {
    //   path: 'merchandise/cmk',
    //   component: CmkTasklistComponent,
    // },
    // {
    //   path: 'merchandise/tops',
    //   component: TopsTasklistComponent,
    // },
    // {
    //   path: 'merchandise/cfm/product',
    //   component: CfmProductComponent,
    // },
    // {
    //   path: 'merchandise/cfr/product',
    //   component: CfrProductComponent,
    // },
    // {
    //   path: 'merchandise/cmk/product',
    //   component: CmkProductComponent,
    // },
    // {
    //   path: 'merchandise/tops/product',
    //   component: TopsProductComponent,
    // },

    // {
    //   path: 'qa',
    //   component: QaTasklistComponent,
    // },
    // {
    //   path: 'qa/product',
    //   component: QaProductComponent,
    // },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
}) 
export class WorklistRoutingModule { }

export const routedComponents = [
  WorklistComponent,

  ProductTemplateComponent,
  TasklistTemplateComponent,

  HistoryListComponent,

  // SupplierTasklistComponent,
  // SupplierProductComponent,

  // BuyerTasklistComponent,
  // BuyerProductComponent,

  // EvpTasklistComponent,
  // EvpProductComponent,

  // MgrTasklistComponent,
  // MgrProductComponent,

  // BysTasklistComponent,
  // BysProductComponent,

  // CfmTasklistComponent,
  // CfrTasklistComponent,
  // CmkTasklistComponent,
  // TopsTasklistComponent,
  // CfmProductComponent,
  // CfrProductComponent,
  // CmkProductComponent,
  // TopsProductComponent,

  // QaTasklistComponent,
  // QaProductComponent,
];
