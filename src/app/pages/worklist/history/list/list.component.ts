import { Component, OnInit, Input, Output, ViewChild, OnDestroy, AfterViewInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder } from "@angular/forms";
import { ViewModel, RequestViewModel } from "./view-model";
import { HttpService, SessionService, DatetimeService, UtilityService, AlertService } from "../../../../shared-core-app/main-core-shared";
import { Subject } from "rxjs";
import { DataTableDirective } from 'angular-datatables';
import {trigger, state, animate, style, transition} from '@angular/animations';

@Component({
  selector: "history-list",
  templateUrl: "./list.component.html",
  styleUrls: ["./list.component.scss"],
  // animations: [
  //   trigger('myInsertRemoveTrigger', [
  //     transition(':enter', [
  //       style({ opacity: 0 }),
  //       animate('1000ms', style({ opacity: 1 })),
  //     ]),
  //     transition(':leave', [
  //       animate('1000ms', style({ opacity: 0 }))
  //     ])
  //   ]),
  // ],
})
export class HistoryListComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild(DataTableDirective) dtElement: DataTableDirective;
  @Input() newTask: boolean = false;
  @Input() moduleName: string = '';
  // @Input() _moduleName: string = '';

  viewModel: ViewModel[] = [];
  checkedRadio: number;
  requestViewModel: RequestViewModel;
  dtTrigger: Subject<ViewModel> = new Subject();
  dtTable: any;

  onFirstLoad: boolean = true;

  constructor(
    public fb: FormBuilder,
    public route: ActivatedRoute,
    public router: Router,
    public httpService: HttpService,
    public sessionService: SessionService,
    public datetimeService: DatetimeService,
    public alertService: AlertService,
    public util: UtilityService,
  ) {}

  ngOnInit() {

    this.route.params.subscribe(routeParams => {
      let data = this.route.snapshot.data;
      this.moduleName = routeParams.moduleName;
      this.onFirstLoad = true;

      if(this.dtElement)
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {          
        dtInstance.destroy();
        this.dtTrigger.next();
      });

      this.initializeData();
      this.getHistoryList();
    });

  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  initializeData() {
    this.requestViewModel = {
      PageSize: 20,
      CurrentPageIndex: 1,
      TotalRows: 0,
      SortDirection: "",
      SortExpression: "",
    };

    this.viewModel = [];
    // this.dtTable = this.util.dataTable(this, {ele: '#dtTable', order: [['created', 'desc']], api: `${this.sessionService.apiUrl}/tasks/supplier?test=${Math.random()}`});

  }

  getHistoryList() {
      this.httpService.HttpGet<any>(`${this.sessionService.apiUrl}/history/process`).subscribe(response => {

        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
          this.viewModel = response;
          this.dtTrigger.next();
        });
  
      });      

    // this.dtTable.order( [[ 3, 'desc' ]] ).draw( false );

  }

  restartHistoryProcess() {
    let itemViewModel:{id: string}[] = [];

    this.util.getCheckedItem().forEach((obj,index) => {
      itemViewModel.push({id : (obj as HTMLInputElement).value});
    });

    //console.log(itemViewModel);
    let id = itemViewModel[0].id;
    
    // this.httpService.HttpPost<any>(`${this.sessionService.apiUrl}/history/process/${id}/restart`, itemViewModel).subscribe(response => this.postSuccess(response));
    this.httpService.HttpPost<any>(`${this.sessionService.apiUrl}/history/process/${id}/restart`, {}).subscribe(response => this.postSuccess(response));

  }

  postSuccess(response) {
    this.alertService.showSuccessMessages();
    this.getHistoryList();
  }
  
}


