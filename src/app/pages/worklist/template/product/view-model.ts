import { BaseRequestModel, BaseViewModel, BaseFileModel } from "../../../../../app/shared-models/base-request-model";

// export interface ResponseViewModel extends BaseRequestModel {
//   Entity: ViewModel[];
// }

export interface RequestViewModel {
	id: string,
	workflow_type: string,
	committee: string,
	committee_week?: string,
	bu: string[],
	country: string,
	product_name_th: string,
	product_name_en: string,
	supplier_code: string,
	supplier_name: string,
	action: string,

	item_characteristics?: string[];
	special_purposes?: string[];
	local_import?: string;
	province?: string;

	distinctive?: string;
}