import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, OnDestroy } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from "@angular/forms";
import { RequestViewModel } from "./view-model";
import { ViewModel } from "../tasklist/view-model";
import { HttpService, SessionService, DatetimeService, UtilityService, AlertService } from "../../../../shared-core-app/main-core-shared";
import { Subject } from "rxjs";
import { valueSelectbox } from '../../../../../../src/app/shared-models/selectbox-view-model';

@Component({
  selector: "product-template",
  templateUrl: "./product.component.html",
  styleUrls: ["./product.component.scss"],  
})
export class ProductTemplateComponent implements OnInit, OnDestroy {
  @Input() formKey: string;
  @Input() formHeader: string = "New Product";
  @Input() moduleName: string = '';
  @Input() _moduleName: string = '';

  myForm: FormGroup;
  viewModel: ViewModel;
  checkedRadio: number;
  requestViewModel: RequestViewModel;
  selectedData: any[] = [];
  viewMode: boolean = false;
  storageKey: string;

  merchandise: boolean = false;

  bu = [
    { name: 'CFM', value: 'CFM' },
    { name: 'CFR', value: 'CFR' },
    { name: 'CMK', value: 'CMK' },
  ];

  committeeItems: valueSelectbox[] = [];
  item_characteristics: valueSelectbox[] = [
    {name: "Bulk Pack", value: "Bulk Pack"},
    {name: "CMK_Exclusive", value: "CMK_Exclusive"},
    {name: "Community Product - CRC_CSR", value: "Community Product - CRC_CSR"},
    {name: "Export Product", value: "Export Product"},
    {name: "None", value: "None"},
    {name: "OTOP Register", value: "OTOP Register"},
    {name: "OTOP Unregister", value: "OTOP Unregister"},
    {name: "Seasonal Product", value: "Seasonal Product"},
    {name: "SME Register", value: "SME Register"},
    {name: "SME Unregister", value: "SME Unregister"},
    {name: "Tourist Product", value: "Tourist Product"},
  ];
  special_purposes: valueSelectbox[] = [
    {name: "Environment/ Welfare - Fair Trade", value: "Environment/ Welfare - Fair Trade"},
    {name: "Environment/ Welfare - Natural", value: "Environment/ Welfare - Natural"},
    {name: "Environment/ Welfare - Organic", value: "Environment/ Welfare - Organic"},
    {name: "Environment/ Welfare - Substantial Resources", value: "Environment/ Welfare - Substantial Resources"},
    {name: "Geographical Indications : GI", value: "Geographical Indications : GI"},
    {name: "Halal", value: "Halal"},
    {name: "Health Specific - Day Free", value: "Health Specific - Day Free"},
    {name: "Health Specific - Diabetic", value: "Health Specific - Diabetic"},
    {name: "Health Specific - Egg Free", value: "Health Specific - Egg Free"},
    {name: "Health Specific - Fat Free", value: "Health Specific - Fat Free"},
    {name: "Health Specific - Gluten Free", value: "Health Specific - Gluten Free"},
    {name: "Health Specific - Healthier Choice Logo", value: "Health Specific - Healthier Choice Logo"},
    {name: "Health Specific - Lactose Free", value: "Health Specific - Lactose Free"},
    {name: "Health Specific - Low Crab", value: "Health Specific - Low Crab"},
    {name: "Health Specific - Low Fat", value: "Health Specific - Low Fat"},
    {name: "Health Specific - Low Sodium", value: "Health Specific - Low Sodium"},
    {name: "Health Specific - low Sugar", value: "Health Specific - low Sugar"},
    {name: "Health Specific - Nut Free", value: "Health Specific - Nut Free"},
    {name: "Health Specific - Sodium Free", value: "Health Specific - Sodium Free"},
    {name: "Health Specific - Sugar Free", value: "Health Specific - Sugar Free"},
    {name: "Health Specific - Trans Fat Free", value: "Health Specific - Trans Fat Free"},
    {name: "Health Specific - Vegan", value: "Health Specific - Vegan"},
    {name: "Health Specific - Vegetarian", value: "Health Specific - Vegetarian"},
    {name: "Health Specific - Wheat Free", value: "Health Specific - Wheat Free"},
    {name: "None", value: "None"},
    {name: "Nutrition - Anti Oxidant Content", value: "Nutrition - Anti Oxidant Content"},
    {name: "Nutrition - HighFibre", value: "Nutrition - HighFibre"},
  ];

  constructor(
    public fb: FormBuilder,
    public route: ActivatedRoute,
    public router: Router,
    public httpService: HttpService,
    public sessionService: SessionService,
    public datetimeService: DatetimeService,
    public alertService: AlertService,
    public util: UtilityService,
  ) {}

  ngOnInit() {

    this.myForm = this.fb.group({
      id: null,
      workflow_type: ['New Product', [Validators.required]],
      committee: [{value: null, disabled: this.viewMode}, [Validators.required]],
      bu: this.fb.array([], [Validators.required]),
      country: [{value: null, disabled: this.viewMode}, [Validators.required]],
      product_name_th: [{value: null, disabled: this.viewMode}, [Validators.required]],
      product_name_en: [{value: null, disabled: this.viewMode}, [Validators.required]],
      supplier_code: [{value: null, disabled: this.viewMode}, [Validators.required]],
      supplier_name: [{value: null, disabled: this.viewMode}, [Validators.required]],
      action: null,
    });
   
    this.getAllNumberOfWeek();
    this.initializeData();

    if(this.moduleName != "supplier") this.myForm.addControl("committee_week", new FormControl({value: this.committeeItems[0].value, disabled: this.viewMode}));
    
    this.route.params.subscribe(params => {
      let data = this.route.snapshot.data;
      this.moduleName = params.moduleName;

      if(this.moduleName == "cfm" || this.moduleName == "cfr" || this.moduleName == "cmk" || this.moduleName == "tops"){
        this.merchandise = true;
      }else{
        this.merchandise = false;
      }

      let id = params["id"] || 0;
      if (id) {

        this.storageKey = `${this.moduleName}_${id}`;
        this.viewModel = JSON.parse(localStorage.getItem(this.storageKey)) as ViewModel;
        if(this.viewModel) {
          this.formKey = this.viewModel.formKey;
          this.formHeader = this.viewModel.name;
        }
        else this.onBack();

        if(this.formKey == "SupplierForm1" || this.formKey == "BuyerForm1"){
          this.viewMode = false;
        }else{
          this.viewMode = true;
          this.form.workflow_type.disable();
          this.form.committee.disable();
          this.form.committee_week.disable();
          this.form.bu.disable();
          this.form.country.disable();
          this.form.product_name_th.disable();
          this.form.product_name_en.disable();
          this.form.supplier_code.disable();
          this.form.supplier_name.disable();
        }

        if(this.formKey == "BuyerForm2"){
          this.myForm.addControl("distinctive", new FormControl('true', [Validators.required]));
        } else if(this.formKey == "BuyerForm3"){
          this.myForm.addControl("item_characteristics", new FormControl([], [Validators.required]));
          this.myForm.addControl("special_purposes", new FormControl([], [Validators.required]));
          this.myForm.addControl("local_import", new FormControl(null, [Validators.required]));
          this.myForm.addControl("province", new FormControl(null, [Validators.required]));
        } else if(this.formKey == "MgrForm1" || this.formKey == "BysForm1"){        
          this.myForm.addControl("item_characteristics", new FormControl({value: null, disabled: true}));
          this.myForm.addControl("special_purposes", new FormControl({value: null, disabled: true}));
          this.myForm.addControl("local_import", new FormControl({value: null, disabled: true}));
          this.myForm.addControl("province", new FormControl({value: null, disabled: true}));
        }

        this.httpService.HttpGet<any>(`${this.sessionService.apiUrl}/tasks/${id}/variables`).subscribe(response => {

          if(response.bu) {
            response.bu.forEach(b => { this.buFormArrayList.push(new FormControl({value: b, disabled: this.viewMode})); });
          }
          this.myForm.patchValue(response);
          this.form.id.setValue(id);

          if(this.merchandise){
            if(response[this.moduleName+'_committee_decision'])
              this.form.action.setValue(response[this.moduleName+'_committee_decision']);
            else
              this.form.action.setValue("reject");
          }
        });

        // Test data
        // if(this.moduleName == "supplier"){
        //   this.requestViewModel = {
        //     id: id,
        //     workflow_type: "New Product",
        //     committee: "committee",
        //     bu: ["CFR"],
        //     // bu: ["CFM", "CFR"],
        //     country: "Thailand",
        //     product_name_th: "เบค่อนรมควัน",
        //     product_name_en: "Smoked Bacon",
        //     supplier_code: "S0001",
        //     supplier_name: "Extend IT Resource Co., Ltd.",
        //     action: "submit"
        //   }

        //   this.myForm.patchValue(this.requestViewModel);
        // }

      }else{
        this.onBack();
      }

    });

  }

  ngOnDestroy(): void {
    if(this.storageKey) localStorage.removeItem(this.storageKey);
  }

  initializeData() {
    this.requestViewModel = {
      id: null,
      workflow_type: 'New Product',
      committee: null,
      bu: [],
      country: null,
      product_name_th: null,
      product_name_en: null,
      supplier_code: null,
      supplier_name: null,
      action: null,
    };    

    this.myForm.patchValue(this.requestViewModel);
    // console.log(this.sessionService);

  }

  doSubmit(action) {

    let requestViewModel = {action: action};

    if(action == "submit"){
      // requestViewModel = Object.assign(this.requestViewModel, this.myForm.getRawValue(), {
      //   bu: this.myForm.getRawValue().bu.map((v, i) => (v ? this.bu[i].name : null)).filter(v => v !== null)
      // }, {action: action});
      requestViewModel = Object.assign(requestViewModel, this.myForm.getRawValue(), {action: action});
    }else if(action == "approve" || action == "reject" || action == "cancel"){
      requestViewModel = Object.assign(requestViewModel, this.myForm.value, {action: action});
    }

    console.log('=============== requestViewModel ===============');
    console.log(requestViewModel);

    this.httpService.HttpPost<any>(`${this.sessionService.apiUrl}/tasks/${this.formKey}/submit`, requestViewModel).subscribe(response => {
      this.postSuccess(response);
    });
  }

  doSave() {

    let requestViewModel = Object.assign(this.myForm.value);

    console.log('=============== requestViewModel ===============');
    console.log(requestViewModel);

    this.httpService.HttpPut<any>(`${this.sessionService.apiUrl}/tasks/${this.formKey}/save`, requestViewModel).subscribe(response => {
      this.postSuccess(response);
    });
  }

  formReset(){
    this.myForm.reset();
    this.initializeData();
  }

  postSuccess(response) {
    this.alertService.showSuccessMessages();
    this.onBack();
  }

  onBuChange(e,i) {
    const checkArray: FormArray = this.myForm.get('bu') as FormArray;
    const value = this.bu[i].value;
    if (e.target.checked) {
      checkArray.push(new FormControl(value));
    } else {
      checkArray.controls.forEach((item: FormControl,i) => {
        if (item.value == value) {
          checkArray.removeAt(i);
          return;
        }
      });
    }
  }

  buChecked(value){
    return this.buControlsList.filter(b => {
      return b.value == value
    }).length == 1;
  }

  get form(){
    return this.myForm.controls;
  }

  get buFormArrayList() {
    return this.form.bu as FormArray;
  }

  get buControlsList() {
    return this.buFormArrayList.controls as FormGroup[];
  }

  getAllNumberOfWeek() {

    const zeroPad = (num, places) => String(num).padStart(places, '0');

    for(let i=0; i<=52; i++){
      let today = new Date();
      today.setDate( today.getDate() + (7 * i) );
      //console.log(this.getNumberOfWeek(today));
      this.committeeItems.push({
        name: today.getFullYear().toString().substr(2,2) + "" + zeroPad(this.getNumberOfWeek(today),2), 
        value: today.getFullYear().toString().substr(2,2) + "" + zeroPad(this.getNumberOfWeek(today),2),
      });
    }

    this.committeeItems = this.committeeItems.slice(1,this.committeeItems.length);
    
  }

  getNumberOfWeek(today: Date) {
    const firstDayOfYear = new Date(today.getFullYear(), 0, 1);
    const pastDaysOfYear = (today.getTime() - firstDayOfYear.getTime()) / 86400000;
    return Math.ceil((pastDaysOfYear + firstDayOfYear.getDay()) / 7);
  }

  onBack() {
    this.router.navigate([`pages/worklist/${this.moduleName}`]);
  }
  
}


