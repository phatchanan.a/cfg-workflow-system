import { BaseRequestModel, BaseViewModel, BaseFileModel } from "../../../../../app/shared-models/base-request-model";

// export interface ResponseViewModel extends BaseRequestModel {
//   Entity: ViewModel[];
// }

export interface RequestViewModel extends BaseRequestModel {}

export interface ViewModel extends BaseViewModel {
  id: string,
  name: string,
  assignee: string,
  created: any,
  due: string,
  followUp: string,
  delegationState: string,
  description: string,
  executionId: string,
  owner: string,
  parentTaskId: string,
  priority: number,
  processDefinitionId: string,
  processInstanceId: string,
  taskDefinitionKey: string,
  caseExecutionId: string,
  caseInstanceId: string,
  caseDefinitionId: string,
  suspended: boolean,
  formKey: string,
  tenantId: string,
}
