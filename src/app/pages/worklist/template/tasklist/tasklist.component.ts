import { Component, OnInit, Input, Output, ViewChild, OnDestroy, AfterViewInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder } from "@angular/forms";
import { ViewModel, RequestViewModel } from "./view-model";
import { HttpService, SessionService, DatetimeService, UtilityService } from "../../../../shared-core-app/main-core-shared";
import { Subject } from "rxjs";
import { DataTableDirective } from 'angular-datatables';
import {trigger, state, animate, style, transition} from '@angular/animations';

@Component({
  selector: "tasklist-template",
  templateUrl: "./tasklist.component.html",
  styleUrls: ["./tasklist.component.scss"],
  // animations: [
  //   trigger('myInsertRemoveTrigger', [
  //     transition(':enter', [
  //       style({ opacity: 0 }),
  //       animate('1000ms', style({ opacity: 1 })),
  //     ]),
  //     transition(':leave', [
  //       animate('1000ms', style({ opacity: 0 }))
  //     ])
  //   ]),
  // ],
})
export class TasklistTemplateComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild(DataTableDirective) dtElement: DataTableDirective;
  @Input() newTask: boolean = false;
  @Input() moduleName: string = '';
  // @Input() _moduleName: string = '';

  viewModel: ViewModel[] = [];
  checkedRadio: number;
  requestViewModel: RequestViewModel;
  dtTrigger: Subject<ViewModel> = new Subject();
  dtTable: any;
  storageKey: string;

  merchandise: boolean = false;
  onFirstLoad: boolean = true;

  constructor(
    public fb: FormBuilder,
    public route: ActivatedRoute,
    public router: Router,
    public httpService: HttpService,
    public sessionService: SessionService,
    public datetimeService: DatetimeService,
    public util: UtilityService,
  ) {}

  ngOnInit() {

    this.route.params.subscribe(routeParams => {
      let data = this.route.snapshot.data;
      this.moduleName = routeParams.moduleName;
      this.onFirstLoad = true;
      
      if(this.moduleName == "cfm" || this.moduleName == "cfr" || this.moduleName == "cmk" || this.moduleName == "tops"){
        this.merchandise = true;
      }else{
        this.merchandise = false;
      }

      if(this.dtElement)
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {          
        dtInstance.destroy();
        this.dtTrigger.next();
      });

      $('nb-card').hide().fadeIn(500);

      this.initializeData();
      this.getTaskList();
    });

  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  initializeData() {
    this.requestViewModel = {
      PageSize: 20,
      CurrentPageIndex: 1,
      TotalRows: 0,
      SortDirection: "",
      SortExpression: "",
    };

    this.viewModel = [];
    // this.dtTable = this.util.dataTable(this, {ele: '#dtTable', order: [['created', 'desc']], api: `${this.sessionService.apiUrl}/tasks/supplier?test=${Math.random()}`});

  }

  createNewTask() {
    this.httpService.HttpPost<any>(`${this.sessionService.apiUrl}/process/NewProduct/start`, {}).subscribe(response => {
      setTimeout(() => {
        if(response.id){

        }
        this.getTaskList();
      }, 1000);      
    });
  }

  getTaskList() {
      this.httpService.HttpGet<any>(`${this.sessionService.apiUrl}/tasks/${this.moduleName}`).subscribe(response => {

        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
          this.viewModel = response;
          this.dtTrigger.next();
        });
  
      });      

    // this.dtTable.order( [[ 3, 'desc' ]] ).draw( false );

  }

  getHeaderTaskList() {
    let retStr = '';
    if(this.merchandise){
      retStr += 'Merchandise ';
      retStr += this.moduleName.charAt(0).toUpperCase() + this.moduleName.substr(1,this.moduleName.length);
    }else if(this.moduleName){
      retStr += this.moduleName.charAt(0).toUpperCase() + this.moduleName.substr(1,this.moduleName.length);
    }
    return retStr + ' : task list';
  }

  createNewProduct(obj: ViewModel) {
    // this.storageKey = `${this.moduleName}_${obj.id}`;
    localStorage.setItem(this.storageKey = `${this.moduleName}_${obj.id}`, JSON.stringify(obj));
    // let id = (this.util.getCheckedItem()[0] as HTMLInputElement).value;
    // this.router.navigate([`pages/worklist/${(this._ModuleName ? this._ModuleName+'/':'')}${this.moduleName}/product`], { queryParams: { id: obj.id } });
    this.router.navigate([`pages/worklist/${this.moduleName}/product/${obj.id}`]);
  }
  
}


