import { Component } from '@angular/core';

@Component({
  selector: 'cfg-worklist',
  template: `<router-outlet></router-outlet>`,
})
export class WorklistComponent {
}
