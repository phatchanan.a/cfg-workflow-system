import { Directive, HostListener, ElementRef, Output, Input, EventEmitter } from "@angular/core";
import { FormGroupDirective, ValidationErrors, FormArray, FormGroup } from "@angular/forms";
import { AlertService, DatetimeService, UtilityService } from "../../../src/app/shared-core-app/main-core-shared";

export interface FormConfigModel {
  confirm: boolean;
  operation: string;
  display?: {};
}

export interface FormDisplayModel {
  fieldName: string;
  text: string;
}

@Directive({
  selector: "[appFormValidator]",
})
export class FormValidatorDirective {
  //@Output() callback: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() callback: EventEmitter<any> = new EventEmitter();
  @Input() config: FormConfigModel;

  formConfig: FormConfigModel = {
    confirm: true,
    operation: "save",
    display: {},
  };

  //el: any;
  form: FormGroupDirective;

  constructor(
    //private elementRef: ElementRef,
    public datetimeService: DatetimeService,
    public formGroup: FormGroupDirective,
    public alertService: AlertService,
    // public confirmationDialogService: ConfirmationDialogService,
    public util: UtilityService
  ) {
    this.form = this.formGroup;
    //this.el = this.elementRef.nativeElement;
  }

  @HostListener("ngSubmit", ["$event"])
  onSubmit(event: Event) {
    //debugger;

    this.formConfig = Object.assign(this.formConfig, this.config);
    this.formConfig.display = Object.assign(this.formConfig.display, this.util.display);

    let msgErr: string[] = [];
    let form = this.form.form;
    //this.submitted.emit(true);

    if (form.invalid) {
      Object.keys(form.controls).forEach(key => {
        const controlErrors: ValidationErrors = form.get(key).errors;
        if (controlErrors != null) {
          key = this.stringTransform(key);
          if (controlErrors.required) msgErr.push(key + " Error with the data is required !!!");
          else if (controlErrors.ngbDate) {
            if (controlErrors.ngbDate.requiredBefore) msgErr.push(key + " Error with the data of date must be start on " + this.datetimeService.setNgbDateStructEN(controlErrors.ngbDate.requiredBefore));
            else if (controlErrors.ngbDate.requiredAfter) msgErr.push(key + " Error with the data of date cannot be more than " + this.datetimeService.setNgbDateStructEN(controlErrors.ngbDate.requiredAfter));
          }
        }
      });

      // Check the form array validator
      Object.keys(form.controls)
        .map(e => {
          return form.controls[e];
        })
        .forEach((e, i) => {
          if ((e as FormArray).controls) {
            if (e.invalid) {

              const controlErrors: ValidationErrors = e.errors;
              let key = this.stringTransform(Object.keys(form.controls)[i]);

              if (controlErrors != null && controlErrors.minLengthArray) msgErr.push(key + " Error with the data is minimum to (" + controlErrors.minLengthArray.min + ") !!!");
              else if (controlErrors != null && controlErrors.maxLengthArray) msgErr.push(key + " Error with the data is maximum to (" + controlErrors.maxLengthArray.max + ") !!!");
              else {
                msgErr.push(key + " Error with the data is incomplete !!!");
                //this.formArrayValidators((e as FormArray),msgErr);
              }

            }
          }
        });
    }

    if (msgErr.length > 0) {
      this.alertService.showWarningMessages(null, msgErr);
      //this.formValid.emit(false);
      return;
    }

    // if (this.formConfig.confirm)
    //   this.confirmationDialogService
    //     .confirm("Please confirm...", "Do you really want to " + this.formConfig.operation + " ?")
    //     .then(confirmed => (confirmed == true ? this.callback.emit(true) : false))
    //     .catch(err => console.log(err));
    // else this.callback.emit(true);
    this.callback.emit(true);
  }

  formArrayValidators(formArray: FormArray, msgErr: string[]) {
    Object.keys(formArray.controls)
      .map(e => {
        return formArray.controls[e];
      })
      .forEach((controls, key) => {
        //console.log((controls as FormGroup).controls);

        Object.keys((controls as FormGroup).controls).forEach(element => {
          //console.log(element);

          const controlErrors: ValidationErrors = (controls as FormGroup).controls[element].errors;
          if (controlErrors != null) {
            Object.keys(controlErrors).forEach(keyError => {
              msgErr.push("[ " + element + " ] at (" + (key + 1) + ") Error with the data is " + keyError);
              //console.log(element +' ['+ (key+1) + '] Error with the data is ' + keyError);
            });
          }
        });
      });
  }

  stringTransform(fieldName) {
    if (this.formConfig.display[fieldName]) return "[ " + this.formConfig.display[fieldName] + " ]";
    else {
      return ("[ " + String(fieldName).charAt(0).toUpperCase() + String(fieldName).replace(/_/g, " ").toLowerCase().slice(1) + " ]");
    }
  }
}
