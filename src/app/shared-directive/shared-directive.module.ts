import { NgModule } from "@angular/core";
import { InputcurrencyDirective, InputtimeDirective, InputFilesDirective, InputuppercaseDirective } from ".";
import { InputdateDirective } from "./inputdate.directive";
import { FormValidatorDirective } from "./form-validator.directive";

@NgModule({
  declarations: [FormValidatorDirective, InputcurrencyDirective, InputtimeDirective, InputFilesDirective, InputuppercaseDirective, InputdateDirective],
  exports: [FormValidatorDirective, InputcurrencyDirective, InputtimeDirective, InputFilesDirective, InputuppercaseDirective, InputdateDirective]
})
export class SharedDirectiveModule {}
