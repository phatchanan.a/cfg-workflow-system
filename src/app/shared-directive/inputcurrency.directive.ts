import { Directive, ElementRef, HostListener, Input, Output, EventEmitter } from "@angular/core";
import { CurrencyPipe } from "@angular/common";

@Directive({
  selector: "[appInputcurrency]"
})
export class InputcurrencyDirective {
  @Output() NUMBER: EventEmitter<string> = new EventEmitter<string>();
  @Input() fraction: number;
  @Input() max: number = 9;
  @Input() min: number = 0;
  el: any;
  constructor(public elementRef: ElementRef, private currencyPipe: CurrencyPipe) {
    this.el = elementRef.nativeElement;
  }

  @HostListener("focus", ["$event.target.value", "$event"])
  onfocus(value, event) {
    const NUMBERS_ONLY = String(value).replace(/[,]/g, "");
    this.el.value = NUMBERS_ONLY;
    this.el.select();
  }

  @HostListener("blur", ["$event.target.value"])
  onblur(value) {
    if (this.fraction == 2) {
      this.el.value = this.currencyPipe.transform(value, "", "", "1.2-2");
    } else if (this.fraction == 4) {
      this.el.value = this.currencyPipe.transform(value, "", "", "1.4-4");
    } else if (this.fraction == 5) {
      this.el.value = this.currencyPipe.transform(value, "", "", "1.5-5");
    } else {
      this.el.value = this.currencyPipe.transform(value, "", "", "1.0");
    }
    this.NUMBER.emit(this.el.value);
  }

  @HostListener("keypress", ["$event"])
  onkeydown(event) {
    // const pattern = /[0-9\+\.\ ]/;
    const pattern = new RegExp('^[0-9\+\.]$');
    let inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      return false;
    }
  }

  @HostListener("keypress", ["$event"])
  numberAndDot(event) {
    // const pattern = /[0-9\+\.\ ]/;
    const pattern = new RegExp('^[0-9\+\.]$');
    let inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      return false;
    }
  }
}
