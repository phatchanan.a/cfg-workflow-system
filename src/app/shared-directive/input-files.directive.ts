import { Directive, Output, HostListener, EventEmitter, Input } from "@angular/core";

@Directive({
  selector: "[appInputFiles]",
})
export class InputFilesDirective {
  @Output() FILES_NAME: EventEmitter<string> = new EventEmitter<string>();
  @Output() FILES_UPLOAD: EventEmitter<FormData> = new EventEmitter<FormData>();
  @Input() FILE_OBJECT_NAME: string = "files";

  constructor() {}

  @HostListener("change", ["$event.target.files"])
  change(files: FileList) {
    if (files.length <= 0) {
      return false;
    }

    let formData = new FormData();
    let count = files.length;
    let files_name = [];

    for (let index = 0; index <= count; index++) {
      if (typeof files[index] != "undefined") {
        let file: File = files[index];

        if (file) {
          files_name.push(file.name);
          formData.append(this.FILE_OBJECT_NAME, file, file.name);
        }
      }
    }

    this.FILES_NAME.emit(files_name.join(","));
    this.FILES_UPLOAD.emit(formData);
  }
}
