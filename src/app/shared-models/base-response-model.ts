export interface BaseResponseModel {
  Entity?: any;
  Errors?: any;
  IsAuthenicated?: boolean;
  ReturnMessage?: any;
  ReturnStatus?: boolean;
  PageSize?: number;
  Token?: string;
  TotalPages?: number;
  TotalRows?: number;
  CurrentPageIndex?: number;
}
