import { BaseResponseModel } from "./base-response-model";

export interface ResponseSelectbox extends BaseResponseModel {
  Entity: valueSelectbox[];
}

export interface valueSelectbox {
  name: any;
  value: any;
}
