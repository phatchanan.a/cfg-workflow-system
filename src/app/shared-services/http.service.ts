import { AlertService } from "./alert.service";
import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse, HttpEventType } from "@angular/common/http";
import { catchError, tap, map } from "rxjs/operators";
import { throwError } from "rxjs";
import { BaseResponseModel } from "../shared-models/base-response-model";

@Injectable({
  providedIn: "root"
})
export class HttpService {
  apiUrl: string;

  constructor(private httpClient: HttpClient, private alertService: AlertService) { }

  HttpGet<T>(url: string) {
    return this.httpClient.get<T>(url).pipe(
      tap(results => console.log(results)),
      catchError(err => this.handleError(err))
    );
  }

  HttpPost<T>(url: string, data: any) {
    return this.httpClient.post<T>(url, data).pipe(
      tap(results => console.log(results)),
      catchError(err => this.handleError(err))
    );
  }

  HttpPut<T>(url: string, data: any) {
    return this.httpClient.put<T>(url, data).pipe(
      tap(results => console.log(results)),
      catchError(err => this.handleError(err))
    );
  }

  DownloadFileByPost<T>(url: string, data: any) {
    this.httpClient.post(url, data, { responseType: "blob", observe: "response" }).subscribe(response => {
      const filename = response.headers
        .get("content-disposition")
        .split(/[;'=]/)
        .pop();

      const downloadedFile = new Blob([response.body], { type: response.body.type });
      const browserName = window.navigator.userAgent;
      console.log(browserName);
      console.log(window.navigator.msSaveOrOpenBlob);
      console.log(decodeURIComponent(filename));
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(downloadedFile, decodeURIComponent(filename));
      } else {
        const a = document.createElement("a");
        a.setAttribute("style", "display:none;");
        document.body.appendChild(a);
        a.download = decodeURIComponent(filename);
        a.href = URL.createObjectURL(downloadedFile);
        a.target = "_blank";
        a.click();
        document.body.removeChild(a);
      }
    });
  }

  UploadFiles<T>(url: string, data: any) {
    return this.httpClient
      .post<T>(url, data, {
        reportProgress: true,
        observe: "events"
      })
      .pipe(
        tap(results => console.log(results)),
        map(event => {
          switch (event.type) {
            case HttpEventType.UploadProgress:
              const progress = Math.round((100 * event.loaded) / event.total);
              return { status: "progress", message: progress };

            case HttpEventType.Response:
              return event.body;
            default:
              return `Unhandled event: ${event.type}`;
          }
        }),
        catchError(err => this.handleError(err))
      );
  }

  handleError(responseError: HttpErrorResponse) {debugger
    let error = responseError.error;
    let messages: string[] = [];

    let header: string = responseError.name;
    let message: string = responseError.message;

    if (error) {
      // let response: BaseResponseModel = responseError.error;
      if(String(error.status).substr(0,1) == "5"){
        header = error.error;
        message = error.message;
      }else if(String(error.status).substr(0,1) == "4"){
        header = responseError.error.error;
        if(error.errors && error.errors.length >= 1)
        message = responseError.error.errors[0].codes[0];
      }

      // messages = responseError.error.message;

      // if (typeof message == "undefined") {
      //   // messages = [];
      //   // messages[0] = JSON.stringify(responseError.error.errors);
      // }

    }
    
    this.alertService.showErrorMessages(header, [message]);
    return throwError(responseError);
  }
}
