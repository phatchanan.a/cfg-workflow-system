import { Injectable } from "@angular/core";
import { formatDate } from "@angular/common";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";

@Injectable({
  providedIn: "root"
})
export class DatetimeService {
  constructor() {}
  CURRENT_DATE_OBJECT = new Date();
  CURRENT_DATE_TIME_EXPORT = formatDate(this.CURRENT_DATE_OBJECT, "yyyyMMddHHmmss", "en");
  CURRENT_DATE_FOR_SEARCH = formatDate(this.CURRENT_DATE_OBJECT, "yyyy-MM-dd", "en");
  CURRENT_DATE = formatDate(this.CURRENT_DATE_OBJECT, "yyyy-MM-dd", "en");
  CURRENT_DATE_TIMESTAMP = formatDate(this.CURRENT_DATE_OBJECT, "yyyy-MM-dd HH:mm:ss", "en");
  CURRENT_TIME = this.CURRENT_DATE_OBJECT.getHours() + ":" + this.CURRENT_DATE_OBJECT.getMinutes() + ":" + this.CURRENT_DATE_OBJECT.getSeconds();

  public setDateISOString(data) {
    if (!data || data == "") {
      return null;
    }

    return new Date(data).toISOString();
  }

  public setNgbDateStructISO(data) {
    if (!data || data == "") {
      return null;
    }

    let date: NgbDateStruct = data;

    return new Date(`${date.year}-${date.month}-${date.day}`).toISOString();
  }

  public setNgbDateStructEN(data) {
    if (!data || data == "") {
      return null;
    }

    let date: NgbDateStruct = data;
    let dateymd = `${date.year}-${date.month}-${date.day}`;
    return formatDate(dateymd, "yyyy-MM-dd", "en");
  }

  public getNgbDateStructEN(data) {
    if (!data || data == "") {
      return null;
    }

    let date = formatDate(data, "yyyy-MM-dd", "en").split("-");
    return { year: Number(date[0]), month: Number(date[1]), day: Number(date[2]) };
  }

  public getDateTime(data) {
    if (!data || data == "") {
      return null;
    }

    return formatDate(data, "yyyy-MM-dd HH:mm:ss", "en");
  }

  public getDateObject(data) {
    if (!data || data == "") {
      return null;
    }

    return new Date(data);
  }

  public getDate(data) {
    if (!data || data == "") {
      return null;
    }

    return formatDate(data, "yyyy-MM-dd", "en");
  }

  public getTime(data) {
    if (!data || data == "") {
      return null;
    }

    return formatDate(data, "HH:mm", "en");
  }

  public setTime(data) {
    if (!data || data == "") {
      return null;
    }

    return " " + data + ":00";
  }

  public getDateByTimeStamp(data) {
    if (!data || data == "") {
      return null;
    }

    let date = new Date(data);
    return formatDate(date.toISOString(), "yyyy-MM-dd", "en");
  }

  public validTime(TimeVal) {
    if (!TimeVal || TimeVal == "") {
      return true;
    }

    let time = TimeVal.split(":");
    let time_start = time[0] || null;
    let time_middle = time[1] || null;
    let regex = /^0[0-9]|1[0-9]|2[0-3]:[0-5][0-9]$/;
    let regex2 = /[0-5][0-9]$/;

    if (!regex.test(TimeVal)) {
      return false;
    }

    if (time_start && Number(time_start) > 23) {
      return false;
    }

    if (!regex2.test(TimeVal)) {
      return false;
    }

    if (time_middle && Number(time_middle) > 59) {
      return false;
    }

    return true;
  }

  public setFormatDateWithTime(DateVal, TimeVal, seconds = "00") {
    let DateTimeValue = DateVal;
    let start_time = seconds == "00" ? " 00:00:" : " 23:59:";

    if (DateVal && TimeVal) {
      DateTimeValue = DateVal + " " + TimeVal + ":" + seconds;
    } else if (!TimeVal || TimeVal == "") {
      if (DateVal && DateVal != "") {
        DateTimeValue = DateVal + start_time + seconds;
      } else {
        DateTimeValue = DateVal;
      }
    } else {
      if (DateVal && DateVal != "") {
        DateTimeValue = DateVal + " :" + seconds;
      } else {
        DateTimeValue = DateVal;
      }
    }

    return DateTimeValue;
  }

  public setWorkingDays(startDate: Date, daysToAdd: number) {
    let dow = startDate.getDay();
    if (dow == 0) {
      daysToAdd++;
    }

    if (dow + daysToAdd >= 6) {
      let remainingWorkDays = daysToAdd - (5 - dow);
      daysToAdd += 2;

      if (remainingWorkDays > 5) {
        daysToAdd += 2 * Math.floor(remainingWorkDays / 5);
        if (remainingWorkDays % 5 == 0) daysToAdd -= 2;
      }
    }

    startDate.setDate(startDate.getDate() + daysToAdd);
    let lastDate = this.getDate(startDate);
    return this.getNgbDateStructEN(lastDate);
  }
}
