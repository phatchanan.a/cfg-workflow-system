import { Injectable } from "@angular/core";
import { HttpHandler, HttpInterceptor, HttpRequest, HttpEvent } from "@angular/common/http";
import { Observable } from "rxjs";
import { finalize } from "rxjs/operators";
import { LoadingScreenService } from "./loading-screen.service";
import { SessionService, DatetimeService } from "../shared-core-app/main-core-shared";

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {
  activeRequest: number = 0;

  constructor(public loadingScreenService: LoadingScreenService, private sessionService: SessionService, private datetimeService: DatetimeService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.sessionService.userViewModel && this.sessionService.userViewModel.Token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${this.sessionService.userViewModel.Token}`,
          ORGANIZE_ID: String(this.sessionService.userViewModel.OrganizaId)
        }
      });

      if (request.body) {
        request.body.CREATED_DATE = this.datetimeService.CURRENT_DATE_TIMESTAMP;
        request.body.CREATED_BY = this.sessionService.userViewModel.UserName;
        request.body.UPDATED_DATE = this.datetimeService.CURRENT_DATE_TIMESTAMP;
        request.body.UPDATED_BY = this.sessionService.userViewModel.UserName;
        request.body.ORGANIZE_ID = this.sessionService.userViewModel.OrganizaId;
      }

      console.log(`Get resource data by currentUser => ${JSON.stringify(this.sessionService.userViewModel)}`);
    } else {
      console.log(`Get resource data by have not currentUser`);
    }

    if (this.activeRequest >= 0) {      
      this.loadingScreenService.startLoading();
      console.log(`Start request is ${this.activeRequest}`);
    }

    this.activeRequest++;

    return next.handle(request).pipe(
      finalize(() => {
        this.activeRequest--;
        console.log(`Request amount is ${this.activeRequest}`);
        if (this.activeRequest === 0) {
          this.loadingScreenService.stopLoading();
        }
      })
    );
  }
}
