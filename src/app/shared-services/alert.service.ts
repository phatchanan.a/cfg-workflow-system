import { Injectable, Type } from '@angular/core';
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService,
  NbToastrConfig,
} from '@nebular/theme';

declare type custom = {
  basic: NbComponentStatus, 
  primary: NbComponentStatus, 
  success: NbComponentStatus, 
  warning: NbComponentStatus, 
  danger: NbComponentStatus, 
  info: NbComponentStatus, 
  control: NbComponentStatus,
};


@Injectable({
  providedIn: "root"
})
export class AlertService {
  constructor(private toastrService: NbToastrService) {}

  config: NbToastrConfig;

  index = 1;
  destroyByClick = true;
  duration = 5000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;

  title = 'HI there!';
  content = `I'm cool toaster!`;

  type: custom = {
    basic: 'basic', 
    primary: 'primary', 
    success: 'success', 
    warning: 'warning', 
    danger: 'danger', 
    info: 'info', 
    control: 'control',
  };

  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];

  quotes = [
    { title: null, body: 'We rock at Angular' },
    { title: null, body: 'Titles are not always needed' },
    { title: null, body: 'Toastr rock!' },
  ];

  makeToast() {
    this.showToast(this.type.primary, this.title, this.content);
  }

  showSuccessMessages(title = "Success" ,content = "Opertion has been success."){
    this.showToast(this.type.success, title, content);
  }

  showWarningMessages(title, content: string[]){
    content.forEach(msg => {
      this.showToast(this.type.warning, "Warning !!!" + (title ? " : " + title:""), msg);      
    });
  }

  showErrorMessages(title, content: string[]){
    content.forEach(msg => {
      this.showToast(this.type.danger, "Error !!!" + (title ? " : " + title:""), msg);    
    });    
  }

  // openRandomToast () {
  //   const typeIndex = Math.floor(Math.random() * this.types.length);
  //   const quoteIndex = Math.floor(Math.random() * this.quotes.length);
  //   const type = this.types[typeIndex];
  //   const quote = this.quotes[quoteIndex];

  //   this.showToast(type, quote.title, quote.body);
  // }

  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    // const titleContent = title ? `. ${title}` : '';
    const titleContent = title ? `${title}` : '';

    this.index += 1;
    this.toastrService.show(
      body,
      // `Toast ${this.index}${titleContent}`,
      `${titleContent}`, config);
  }
}
