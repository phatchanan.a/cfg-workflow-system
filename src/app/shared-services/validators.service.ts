import { Injectable } from "@angular/core";
import { HttpService } from "./http.service";
import { SessionService } from "./session.service";
import { AbstractControl } from '@angular/forms';

@Injectable({
  providedIn: "root",
})
export class ValidatorsService {

  constructor(public httpService: HttpService, public sessionService: SessionService) {}

  minLengthArray(min: number) {
    return (c: AbstractControl): { [key: string]: any } => {
      if (c.value.length >= min) return null;

      return { minLengthArray: { valid: false, min: min } };
    };
  }

  maxLengthArray(max: number) {
    return (c: AbstractControl): { [key: string]: any } => {
      if (c.value.length <= max) return null;

      return { maxLengthArray: { valid: false, max: max } };
    };
  }

}
