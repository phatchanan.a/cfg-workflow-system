import { Injectable } from "@angular/core";
import { HttpService } from "./http.service";
import { SessionService } from "./session.service";
import { isNumber } from "util";
import { ValidatorsService } from './validators.service';

@Injectable({
  providedIn: "root",
})
export class UtilityService {
  public display = { };

  constructor(public httpService: HttpService, public sessionService: SessionService, public validator: ValidatorsService) {}

  public dataTable(refElem, config) {
    if (!config.columnDefs) config.columnDefs = [{ targets: 0, orderable: false }];
    if (!config.requestViewModel) config.requestViewModel = "requestViewModel";
    if (!config.viewModel) config.viewModel = "viewModel";

    if (config.order && config.order.length != 0) {
      $(config.ele)
        .DataTable()
        ["context"][0].aoColumns.forEach(e => {
          if ($(e.nTh).attr("name") == config.order[0][0]) {
            config.order[0][0] = e.idx;
            return;
          }
        });

      if (!isNumber(config.order[0][0])) {
        config.order[0][0] = 1;
        config.order[0][1] = "asc";
      }
    }

    let MAIN_TABLE = $(config.ele).DataTable({
      // scrollY: '300px',
      scrollX: true,
      autoWidth: false,
      deferRender: true,
      lengthChange: false,
      searching: false,
      info: true,
      //pagingType: 'full_numbers',
      //paging: false,
      //dom:'<"top"i>tr<"bottom">ip',
      pageLength: refElem[config.requestViewModel].PageSize == null ? 20 : refElem[config.requestViewModel].PageSize,
      responsive: true,
      serverSide: true,
      // processing: true,
      deferLoading: 0,
      destroy: true,
      ajax: (data, callback, settings) => {
        // debugger;

        // if (config.order && config.order.length != 0) {
        //   let ele = $(config.ele)
        //     .DataTable()
        //     .columns()["context"][0].aaSorting[0];
        //   refElem[config.requestViewModel].SortExpression = $($($(config.ele).DataTable()["context"][0].aoColumns[ele[0]])[0].nTh).attr("name");
        //   refElem[config.requestViewModel].SortDirection = ele[1];
        // }
        // refElem[config.requestViewModel].CurrentPageIndex =
        //   $(config.ele)
        //     .DataTable()
        //     .page.info().page + 1;

        if (config.extFn) {
          config.extFn(refElem, callback);
        } else
          // this.httpService.HttpPost<any>(`${refElem.sessionService.apiUrl}/${!config.api ? "Search" : config.api}`, refElem[config.requestViewModel]).subscribe(response => {
          // this.httpService.HttpGet<any>(`http://localhost:4200/src/assets/data/news.json`).subscribe(response => {
          this.httpService.HttpGet<any>(config.api).subscribe(response => {
            //refElem.apiSuccess(response);

            // $('.container-fluid').css('min-height', $('.container-fluid').height());

            if (config.extCb) {
              config.extCb(refElem,response);
            } else {
              refElem[config.viewModel] = response;
              refElem[config.requestViewModel].TotalRows = response.length || 0;
              refElem[config.requestViewModel].PageSize = refElem[config.requestViewModel].PageSize;
            }

            callback({
              recordsTotal: response.length || 0,
              recordsFiltered: response.length || 0,
              data: []
            });
            setTimeout(function(){
              $(config.ele).DataTable().columns.adjust();
            }, 10);
          });
      },
      drawCallback: function( settings ) {
        $('div.dataTables_scrollHeadInner').css('width','');
        setTimeout(function(){
          $(config.ele).DataTable().columns.adjust();
          // $('.container-fluid').css('min-height', '');

          if($(config.ele + '_wrapper .dataTables_scrollBody').get(0).scrollWidth - 1 > $(config.ele + '_wrapper .dataTables_scrollBody').innerWidth()){ 
            $('.dataTables_scrollHeadInner table.dataTable tr').find('th').each((i,e) => {
              $(e).css('min-width', $(e).css('width'));
            });
          }

          $('div.dataTables_scrollHeadInner').css('width','');
          $('div.dataTables_scrollHeadInner').css('padding-right','0px');          
          // $('div.dataTables_scrollBody table thead tr').css('height','30px');
        }, 80);
      },
      order: config.order,
      columnDefs: config.columnDefs,
      ordering: config.ordering != null ? config.ordering : true,
    });

    $(window).on('resize', function () {
      $(config.ele).DataTable().columns.adjust();

      if($(config.ele + '_wrapper .dataTables_scrollBody').get(0).scrollWidth - 1 > $(config.ele + '_wrapper .dataTables_scrollBody').innerWidth()){ 
        $('.dataTables_scrollHeadInner table.dataTable tr').find('th').each((i,e) => {
          $(e).css('min-width', $(e).css('width'));
        });
      }

      $('div.dataTables_scrollHeadInner').css('width','');
      $('div.dataTables_scrollHeadInner').css('padding-right','0px');          
      // $('div.dataTables_scrollBody table thead tr').css('height','30px');
    } );

    return MAIN_TABLE;
  }

  public GetDtOptions(refElem, config) {
    if (!config.columnDefs) config.columnDefs = [{ targets: 0, orderable: false }];
    if (!config.requestViewModel) config.requestViewModel = "requestViewModel";

    let dtOptions = {
      lengthChange: false,
      searching: false,
      info: true,
      pageLength: refElem[config.requestViewModel].PageSize == null ? 20 : refElem[config.requestViewModel].PageSize,
      processing: true,
      retrieve: true,
      order: config.order,
      columnDefs: config.columnDefs,
      ordering: config.ordering != null ? config.ordering : true,
      language: {
        paginate: {
          next: '<i class="fas fa-arrow-alt-circle-right"></i>',
          previous: '<i class="fas fa-arrow-alt-circle-left"></i>'
        }
      },
      drawCallback: function( settings ) {
        if(!refElem['onFirstLoad']) $('#dtTable').hide().fadeIn(500); 
        setTimeout(() => {
          refElem['onFirstLoad'] = false;
        }, 600);       
      },
      
    };

    return dtOptions;
  }
  
  // ==========================================================================================================================================================

  public array_uniq(values) {
    let concatArray = values.map(eachValue => {
      return Object.keys(eachValue)
        .map(function(e) {
          return eachValue[e];
        })
        .join("");
    });
    let filterValues = values.filter((value, index) => {
      return concatArray.indexOf(concatArray[index]) === index;
    });
    return filterValues;
  }

  public strLenWithLimit(str: string, len = 50){
    if(str.length > len) return str.substr(0,len) + '...';
    else return str;
  }

  // ==========================================================================================================================================================

  public isDisabledButton(multiple = false, table_id = "#dtTable"): boolean {
    let length = this.getCheckedItem(table_id).length;

    if (!multiple) {
      return length == 1 ? false : true;
    } else {
      return length >= 1 ? false : true;
    } 

  }

  public isCheckedAll(table_id = "#dtTable"): boolean {
    let isCheckAll: boolean = this.getCheckedItem(table_id).length == this.getCheckeBoxItem(table_id).length && this.getCheckedItem(table_id).length != 0;
    if (isCheckAll) return true;
    else return false;
  }

  public getCheckedItem(table = "#dtTable") {
    return Array.from(document.querySelectorAll("table" + table + " tr td input[name=CHECKBOX]:checked"));
  }

  public getCheckeBoxItem(table = "#dtTable") {
    return Array.from(document.querySelectorAll("table" + table + " tr td input[name=CHECKBOX]"));
  }

  public getCheckeBoxAllItem(table = "#dtTable") {
    return document.querySelector("table" + table + " tr th input[name=ALL_CHECKBOX]") as HTMLInputElement;
  }

  public CheckAllItem(e, formArr = null) {
    let table_id = "#" + (e.currentTarget.closest("table").id ? e.currentTarget.closest("table").id : "dtTable");
    if (this.getCheckeBoxItem(table_id).length == 0) {
      this.getCheckeBoxAllItem(table_id).checked = false;
      return;
    }

    let checkAll: boolean = this.getCheckedItem(table_id).length < this.getCheckeBoxItem(table_id).length ? true : false;
    this.getCheckeBoxItem(table_id).forEach((obj, index) => {
      (obj as HTMLInputElement).checked = checkAll;
      checkAll ? obj.closest("tr").classList.add("rows-active") : obj.closest("tr").classList.remove("rows-active");
    });

    if (formArr)
      formArr.forEach(e => {
        if (e.controls.CHECKBOX.enabled) e.controls.CHECKBOX.setValue(checkAll);
      });
  }

  public CheckItem(e, multiSelect = true, form = null) {
    if (!e.srcElement.closest("tr").querySelector('input[name="CHECKBOX"]')) return;

    if (e.srcElement.type) {
      if (e.srcElement.type != "checkbox" || (e.srcElement.name != "CHECKBOX" && e.srcElement.name != "ALL_CHECKBOX")) return;
    }

    if (
      e.srcElement
        .closest("tr")
        .querySelector('input[name="CHECKBOX"]')
        .classList.contains("d-none")
    )
      return;

    // -------------------------------------------------------

    let table_id = e.currentTarget.closest("table").id;
    if (!table_id || table_id == "") return;
    table_id = "#" + table_id;

    //---------------- multiple checked false ----------------
    if (!multiSelect)
      this.getCheckedItem(table_id).forEach((obj, index) => {
        if (e.currentTarget.querySelector("td > input[name=CHECKBOX]") != obj) {
          (obj as HTMLInputElement).checked = false;
          obj.closest("tr").classList.remove("rows-active");
        }
      });

    if (e) {
      if (e.srcElement.closest("tr>td")) e.currentTarget.classList.toggle("rows-active");
      if (e.srcElement.type != "checkbox") e.currentTarget.querySelector("td > input[name=CHECKBOX]").checked ^= 1;
      if (e.srcElement.type != "checkbox" && form) form.controls.CHECKBOX.setValue(form.controls.CHECKBOX.value ? false : true);
    }
  }

}
