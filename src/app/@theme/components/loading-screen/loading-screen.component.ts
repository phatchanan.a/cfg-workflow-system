import { Component, OnInit, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";
import { LoadingScreenService } from "../../../shared-services/loading-screen.service";
import { debounceTime } from "rxjs/operators";

@Component({
  selector: "app-loading-screen",
  templateUrl: "./loading-screen.component.html",
  styleUrls: ["./loading-screen.component.scss"]
})
export class LoadingScreenComponent implements OnInit, OnDestroy {
  public loading: boolean;
  loadingSubscription: Subscription;

  constructor(public loadingScreenService: LoadingScreenService) {}

  ngOnInit() {
    this.loading = false;
    this.loadingSubscription = this.loadingScreenService.loadingStatus.subscribe(value => (this.loading = value));
  }

  ngOnDestroy() {
    this.loadingSubscription.unsubscribe();
  }
}
